FROM node:16-alpine3.15

WORKDIR /app

COPY package.json yarn.lock /app/

RUN yarn install

COPY . .

RUN yarn build

CMD [ "node", "dist/main.js" ]